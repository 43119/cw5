<?php
require_once "vendor/autoload.php";

use MyApp\Application;

// app run
$app = new Application();
$app->run();

